#### 介绍
基于Vue/JqueryMobile+WebPack的Web聊天室

#### 软件架构
| 软件       | 版本    | 功能           | 地址                             |
|----------|-------|--------------|--------------------------------|
| Nodejs | 14.15.3   | 一个基于 Chrome V8 引擎的 JavaScript 运行环境 | https://nodejs.org/en/|
| Vue   | 2.9.6 | 一套构建用户界面的渐进式框架 | https://vuejs.org/ |
| Jquery.mobile   | 1.4.5 | 是创建移动 web 应用程序的框架 | https://jquerymobile.com/ |
| Express | 4.17.1 | 基于 Node.js 平台，快速、开放、极简的 Web 开发框架 | https://www.expressjs.com.cn/ |

#### 文件夹说明
| 文件夹 | 作用 |
|----------|-------|
| chat | express+socket后台 |
| chat/public | vue build后生成的文件 |
| chat_client | Vue前端 |
| chat_client/static | Vue前端的资源文件夹，其中包含了jqueryMobile的页面 |
#### 开发：安装教程

1.  cd chat&&npm install
2.  cd chat_client&&npm install

#### 部署：安装教程

1.  cd chat&&npm install

#### 开发：使用说明

1.  mysql创建chat数据库，修改chat文件夹下util中的chat.js中关于数据库的用户名和密码的值，并且将chat.sql文件导入chat数据库中。
2.  cd chat_client&&npm start
3.  cd chat&&npm start

#### 部署：使用说明

1.  mysql创建chat数据库，修改chat文件夹下util中的chat.js中关于数据库的用户名和密码的值，并且将chat.sql文件导入chat数据库中。
2.  cd chat_client&&npm start

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
