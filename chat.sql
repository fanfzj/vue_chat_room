﻿# Host: localhost  (Version: 5.7.26)
# Date: 2021-05-27 09:42:41
# Generator: MySQL-Front 5.3  (Build 4.234)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "chat_admin"
#

DROP TABLE IF EXISTS `chat_admin`;
CREATE TABLE `chat_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_account` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_password` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='管理员表';

#
# Data for table "chat_admin"
#

/*!40000 ALTER TABLE `chat_admin` DISABLE KEYS */;
INSERT INTO `chat_admin` VALUES (1,'admin1234','admin1234');
/*!40000 ALTER TABLE `chat_admin` ENABLE KEYS */;

#
# Structure for table "chat_message"
#

DROP TABLE IF EXISTS `chat_message`;
CREATE TABLE `chat_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `send_user_id` int(11) DEFAULT NULL COMMENT '发送id',
  `receive_user_id` int(11) DEFAULT NULL COMMENT '接受id',
  `content` text CHARACTER SET utf8 COMMENT '内容',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `del` int(11) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`),
  KEY `message_ibfk_1` (`send_user_id`),
  KEY `message_ibfk_2` (`receive_user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=76 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='消息表';

#
# Data for table "chat_message"
#

/*!40000 ALTER TABLE `chat_message` DISABLE KEYS */;
INSERT INTO `chat_message` VALUES (1,2,1,'12345','2021-05-02 17:01:57',0),(2,1,2,'ccc','2021-05-02 19:01:57',0),(3,1,2,'ccc','2021-05-02 18:01:57',0),(4,2,1,'aaaa','2021-05-24 17:56:56',0),(5,1,2,'new','2021-05-24 17:57:16',0),(6,1,2,'new','2021-05-24 17:57:25',0),(7,2,1,'fzj','2021-05-25 13:34:07',0),(8,2,1,'aaa','2021-05-25 13:35:24',0),(9,1,2,'bbb','2021-05-25 13:38:58',0),(10,1,2,'aaa','2021-05-25 13:39:08',0),(11,2,1,'3','2021-05-25 13:55:03',0),(12,1,2,'4','2021-05-25 13:55:30',0),(13,1,2,'5','2021-05-25 13:57:22',0),(14,2,1,'1','2021-05-25 13:59:04',0),(15,1,2,'3','2021-05-25 13:59:19',0),(16,1,2,'1','2021-05-25 14:01:56',0),(17,2,1,'3','2021-05-25 14:06:10',0),(18,2,1,'3','2021-05-25 14:06:38',0),(19,2,1,'3','2021-05-25 14:07:44',0),(20,2,1,'3','2021-05-25 14:08:55',0),(21,1,2,'w','2021-05-25 14:13:40',0),(22,2,1,'c','2021-05-25 14:15:58',0),(23,2,1,'2','2021-05-25 14:16:38',0),(24,1,2,'1111','2021-05-25 14:17:18',0),(25,2,1,'3','2021-05-25 14:19:05',0),(26,1,2,'3','2021-05-25 14:23:02',0),(27,1,2,'2','2021-05-25 14:25:05',0),(28,2,1,'3','2021-05-25 14:26:37',0),(29,1,2,'4','2021-05-25 14:34:47',0),(30,2,1,'5','2021-05-25 14:35:02',0),(31,1,2,'1','2021-05-25 14:36:32',0),(32,1,2,'2','2021-05-25 14:50:14',0),(33,1,2,'3','2021-05-25 14:50:50',0),(34,1,2,'4','2021-05-25 14:50:55',0),(35,1,2,'1','2021-05-25 14:51:14',0),(36,2,1,'2','2021-05-25 14:52:13',0),(37,1,2,'3','2021-05-25 14:52:22',0),(38,1,2,'2','2021-05-25 15:31:48',0),(39,2,1,'3','2021-05-25 15:32:06',0),(40,1,2,'3','2021-05-25 15:39:05',0),(41,1,2,'5','2021-05-25 15:39:09',0),(42,2,1,'5','2021-05-25 15:39:29',0),(43,2,1,'3','2021-05-25 15:49:48',0),(44,2,1,'10','2021-05-25 15:49:53',0),(45,1,2,'4','2021-05-25 15:51:40',0),(46,2,1,'20','2021-05-25 15:51:48',0),(51,1,2,'nihao ','2021-05-26 16:29:47',0),(52,1,2,'1','2021-05-26 16:31:01',0),(56,1,2,'3','2021-05-26 17:00:25',0),(57,1,2,'4','2021-05-26 17:00:48',0),(61,2,1,'111','2021-05-26 17:05:33',0),(63,2,1,'cc','2021-05-26 17:06:32',0),(64,2,1,'444','2021-05-26 17:07:34',0),(73,2,1,'aa','2021-05-26 17:14:07',0),(76,2,1,'111','2021-05-26 17:19:23',0),(77,2,1,'你好','2021-05-26 17:19:31',0),(78,1,2,'2','2021-05-26 17:20:21',0),(79,1,2,'3','2021-05-26 17:20:42',0),(80,1,2,'2','2021-05-26 17:21:22',0),(81,2,1,'4','2021-05-26 17:22:13',0),(82,1,2,'你好','2021-05-26 17:22:39',0),(83,2,1,'hahha','2021-05-26 17:22:44',0),(84,2,1,'44','2021-05-26 17:30:23',0),(85,1,2,'1','2021-05-26 17:30:49',0),(86,2,1,'2','2021-05-26 17:31:45',0);
/*!40000 ALTER TABLE `chat_message` ENABLE KEYS */;

#
# Structure for table "chat_user"
#

DROP TABLE IF EXISTS `chat_user`;
CREATE TABLE `chat_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_account` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '用户名',
  `user_password` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '密码',
  `nickname` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '昵称',
  `head_img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '头像',
  `sex` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '性别',
  `mobile_number` varchar(13) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '手机号',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `del` int(11) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='用户表';

#
# Data for table "chat_user"
#

/*!40000 ALTER TABLE `chat_user` DISABLE KEYS */;
INSERT INTO `chat_user` VALUES (1,'fzj','123456','zimu','static/image/20210506105708.jpg','男','15900792449','2021-05-21 17:01:57',0),(2,'fzj2','123456','zimu2','static/image/20210506105708.jpg','男','11111111111','2021-05-24 17:01:57',0);
/*!40000 ALTER TABLE `chat_user` ENABLE KEYS */;
