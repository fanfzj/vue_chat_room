import Vue from 'vue'
import Router from 'vue-router'
import AdminHome from '@/views/adminHome'
import AdminLogin from '@/views/adminLogin'
import Login from '@/views/Login'
import AdminUserEdit from '@/views/adminUserEdit'
import AdminUserAdd from '@/views/adminUserAdd'
import AdminUserList from '@/views/adminUserList'
import chatWin from '@/views/chatWin'
import chatList from '@/views/chatList'
import MessageCount from '@/views/messagesCount'
Vue.use(Router)
var router = new Router({
  routes: [
    {
      path:'/',
      redirect:'/login',
    },
    {
      path:'/login',
      name: 'Login',
      component: Login
    },
    {
      path:'/chat/list',
      name: 'chatList',
      meta:{
        requireUser:true
      },
      component: chatList
    },
    {
      path:'/chat/message/count',
      name: 'MessageCount',
      meta:{
        requireUser:true
      },
      component: MessageCount
    },
    {
      path:'/chat/win',
      name: 'chatWin',
      meta:{
        requireUser:true
      },
      component: chatWin
    },
    {
      path: '/admin',
      meta:{
        requireAuth:true
      },
      component: AdminHome,
      children:[{
        path:'/',
        redirect:'userlist',
        name: 'AdminHome',
      },{
        path:'userlist',
        name:'userlist',
        component:AdminUserList,
        meta:{
          requireAuth:true
        }
      },{
        path:'useradd',
        name:'useradd',
        component:AdminUserAdd,
        meta:{
          requireAuth:true
        }
      },
        {
          path:'useredit',
          name:'UserEdit',
          component:AdminUserEdit,
          meta:{
            requireAuth:true
          }
        }],
    },
    {
      path: '/admin/login',
      name: 'AdminLogin',
      component: AdminLogin
    }
  ]
})
router.beforeEach((to,from,next)=>{
  if(to.meta.requireAuth){
    //判断当前路由是否有拦截器权限
    if(Vue.prototype.getCookie('adminToken')){
      //判断当前本地是否存在Token
      next()
    }
    else {
      next('/admin/login')
    }
  }
  else if(to.meta.requireUser){
    if(Vue.prototype.getCookie('userToken')){
      //判断当前本地是否存在Token
      next()
    }
    else {
      next('/login')
    }
  }
  else{
    next()
  }
})

export default router;
