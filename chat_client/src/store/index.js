import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
//状态管理数据存放
const state={
  user:'',
}
//状态管理修改方法
const mutations={
  SETSTATE(state,user) {state.user=user;}
}
//状态管理修改请求
const actions = {
  setStateUser(context,user) { context.commit('SETSTATE',user);}
}
//状态管理数据获取
const getters = {
  getState(state) { return state;}
}
//注册到VUex的Store中
export default new Vuex.Store({
  state,
  mutations,
  actions,
  getters
})

