// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'
import store from './store'
Vue.prototype.$axios = axios;

Vue.config.productionTip = false

/* eslint-disable no-new */

//设置Cookie
Vue.prototype.setCookie=function (params){
  let cookieStr = '';
  for (var index in params){
    let exp = new Date();
    exp.setTime(exp.getTime()+30*60*1000);
    document.cookie = index + '=' + params[index] +';expires='+ exp.toGMTString() +';path=/';
  }
}
//获取cookie
Vue.prototype.getCookie=function (params){
  let name = params + '=';
  let ca = document.cookie.split(';');
  for (var i =0;i<ca.length;i++){
    var c = ca[i].trim();
    if(c.indexOf(name) == 0){
      return c.substring(name.length,c.length);
    }
  }
  return '';
}
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  store,
})
