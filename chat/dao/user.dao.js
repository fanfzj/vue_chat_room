const db=require('../util/db.js');
const mysql=require('mysql');
exports.isExist=(user,callback_service)=>{
    let sql=mysql.format("select * from chat_user where user_account = ? and user_password = ?",[user.user_account,user.user_password]);
    db.query(sql,(data)=>{
        callback_service(data);
    });
}
exports.getList=(page,number,callback_service)=>{
    if(page===-1){
        sql='select * from char_user';
    }
    else{
        sql="select * from chat_user limit "+(page-1)*number+","+number;
    }
    db.query(sql,(data,error)=>{
        callback_service(data,error);
    });
}
exports.add=(user,callback_service)=>{
    let sql=mysql.format("insert into chat_user(user_account,user_password,nickname,head_img,sex,mobile_number,del) value (?,?,?,?,?,?,?)",[user.user_account,user.user_password,user.nickname,user.head_img,user.sex,user.mobile_number,user.del]);

    db.query(sql,(data,error)=>{
        callback_service(data,error);
    });
}
exports.getUserAllById=(id,callback_service)=>{
    let sql=mysql.format("select * from chat_user where id=?",id);
    db.query(sql,(data)=>{
       callback_service(data);
    });
}
exports.isRegistered=(user,callback_service)=>{
    let sql=mysql.format("select count(id) as count from chat_user where del=0 and (user_account=? or mobile_number=?)",[user.user_account,user.mobile_number])
    db.query(sql,(data)=>{
        callback_service(data);
    })
}
exports.updateAllUser=(user,callback_service)=>{
    let sql=mysql.format("update chat_user set user_account=?,user_password=?,nickname=?,head_img=?,sex=?,mobile_number=?,del=? where id=?",[user.user_account,user.user_password,user.nickname,user.head_img,user.sex,user.mobile_number,user.del,user.id]);
    db.query(sql,(data)=>{
        callback_service(data);
    })
}
exports.getUserCount=(callback_service)=>{
    let sql="select count(*) as count from chat_user where del=0";
    db.query(sql,(data)=>{
        callback_service(data);
    });
}
exports.delUser=(user_id,callback_service)=>{
    let sql=mysql.format("update chat_user set del=1 where id= ? ",[user_id])
    db.query(sql,(data)=>{
        callback_service(data);
    });
}
exports.restoreUser=(user_id,callback_service)=>{
    let sql=mysql.format("update chat_user set del=0 where id= ? ",[user_id])
    db.query(sql,(data)=>{
        callback_service(data);
    });
}
exports.searchUser=(keyword,callback_service)=>{
    let sql="select * from chat_user where user_account like '%"+keyword+"%' or nickname like '%"+keyword+"%'";
    db.query(sql,(data)=>{
        callback_service(data);
    });
}