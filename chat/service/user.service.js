const userDao = require('../dao/user.dao.js');
const user = require("../model/user.js");
const md5 = require('../util/md5.js');
const file=require('../util/file.js');
//登录
exports.login=(req,callback_controller)=>{
    let model=new user(null,req.body['account'],req.body['password']);
    userDao.isExist(model,(data)=>{
        if(data[0]==null){
            callback_controller(undefined);
        }
        else {
            let token =md5.getToken(data[0].id.toString());
            req.session.userToken=data[0].id.toString();
            req.session.user_id=data[0].id;
            callback_controller(data,{'token':token});
        }
    })
}
//用户列表
exports.getlist=(req,callback_controller)=>{
    page=req.query.page;
    number=req.query.number;
    userDao.getList(page,number,(data,error)=>{
        if(error!=null){
            callback_controller(-1,error);
        }
        else if(data.length===0){
            callback_controller(200,'用户列表为空',data,{"current":0,"total":0});
        }
        else {
            userDao.getUserCount((data_1)=>{
                callback_controller(200,'获取成功',data,{"current":page,"total":Math.ceil(data_1[0].count/number)});
            })
        }
    })
}
//新增用户
exports.add=(req,callback_controller)=>{

    let model=new user(null,req.body['user_account'],req.body['password'],req.body['nickname'],req.body['head_img'],req.body['sex'],req.body['mobile_number'],0);
    if(typeof req.file!=="undefined"){
        model.setHeadImg('public/image/'+req.file.originalname);
    }
    else{
        model.setHeadImg('public/img/default.png');
    }
    userDao.isRegistered(model,(data,error)=>{
        if(error!=null){
            callback_controller(-1,error);
        }
        else if(data[0].count>0){
            userDao.add(model,(data,error)=>{
                if(error!=null){
                    callback_controller(-1,error,data);
                }
                else if(data.affectedRows!==1){
                    if(typeof req.file!=='undefined'){
                        file.remove(req.file.path);
                    }
                    callback_controller(-1,'数据异常',data)
                }
                else{
                    if(typeof req.file!=='undefined'){
                        file.rename(req.file.path,model.getHeadImg(),req.file.originalname);
                    }
                    callback_controller(200,'新增成功',data)
                }
            });
        }
        else{
            if(typeof req.file!=='undefined'){
                file.remove(req.file.path);
            }
            callback_controller(-1,'已注册，不可以重复注册')
        }
    })
}
//获取所有用户信息
exports.getUserAllById=(req,callback_controller)=>{
    let id =req.query.user_id;
    userDao.getUserAllById(id,(data)=>{
        callback_controller(200,"",data)
    });
}
//修改用户信息
exports.updateAllUser=(req,callback_controller)=>{
    let model=new user(req.body['user_id'],req.body['user_account'],req.body['password'],req.body['nickname'],req.body['head_img'],req.body['sex'],req.body['mobile_number'],req.body['del']);
    if(typeof req.file!=="undefined"){
        model.setHeadImg('image/'+req.file.originalname);
    }
    else{
        model.setHeadImg(req.body['old_path']);
    }
    userDao.updateAllUser(model,(data,error)=>{
        if(error!=null){
            callback_controller(-1,error,data);
        }
        else if(data.affectedRows!==1){
           if(typeof req.file!=='undefined'){
               file.remove(req.file.path);
           }
            callback_controller(-1,'数据异常',data);
       }
       else{
           if(typeof req.file!=='undefined'){
               file.rename(req.file.path,model.getHeadImg(),req.file.originalname);
               if(req.body['old_path']!=='images/default.png'){
                   file.remove(req.body['old_path'])
               }
           }
           callback_controller(200,'操作成功',data)
       }
    });
}
//根据用户名或者昵称搜索
exports.searchUser=(req,callback_controller)=>{
    let keyword =req.query.keyword;
    userDao.searchUser(keyword,(data,error)=>{
        if(error!=null){
            callback_controller(undefined,{'error':error});
        }
        else{
            callback_controller(data)
        }
    });
}