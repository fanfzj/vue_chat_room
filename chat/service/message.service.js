const messageDao = require('../dao/message.dao.js');
const message = require("../model/message.js");
//聊天列表
exports.getlist=(req,callback_controller)=>{
    let user_id=req.session.user_id;
    messageDao.getLastMessageList(user_id,(data,error)=>{
        if(error!=null){
            callback_controller(-1,error,data);
        }
        else{
            callback_controller(200,"操作成功",data);
        }
    })
}
//获取信息列表
exports.getMessageByFriend=(req,callback_controller)=>{
    let model=new message(null,req.query.friend_id,req.query.user_id,null);
    messageDao.getMessageByFriend(model,(data,error)=>{
        if(error!=null){
            callback_controller(-1,error,data);
        }
        else{
            callback_controller(200,"操作成功",data);
        }
    })
}
//保存信息
exports.saveMessage=(req,callback_controller)=>{
    let model=new message(null,req.body['user_id'],req.body['friend_id'],req.body['content']);
    messageDao.saveMessage(model,(data,error)=>{
        if(error!=null){
            callback_controller(-1,error,data);
        }
        else if(data.affectedRows!==1){
            callback_controller(-1,'数据异常',data);
        }
        else{
            callback_controller(200,"操作成功",data);
        }
    })
}
//信息统计
exports.getMessageCountByUser=(req,callback_controller)=>{
    let id = req.query.id;
    let time_start = req.query.time_start;
    let time_end = req.query.time_end;
    messageDao.getMessageCountByUser(id,time_start,time_end,(data,error)=>{
        if(error!=null){
            callback_controller(-1,'数据异常',data);
        }
        else{
            callback_controller(200,"操作成功",data);
        }
    })
}