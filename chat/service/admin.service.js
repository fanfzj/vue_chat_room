const adminDao = require('../dao/admin.dao.js');
const admin = require("../model/admin.js");
const md5 = require('../util/md5.js');
exports.login=(req,callback_controller)=>{
    let model=new admin(null,req.body['admin_account'],req.body['admin_password']);
    adminDao.isExist(model,(data,error)=>{
        if(error!=null){
            callback_controller(-1,error,data);
        }
        else if(data.length===0){
            callback_controller(-1,"用户不存在",data);
        }
        else {
            let token =md5.getToken(data[0].id.toString());
            req.session.adminToken=data[0].id.toString();
            callback_controller(200,"操作成功",data,{'token':token});
        }
    })
}