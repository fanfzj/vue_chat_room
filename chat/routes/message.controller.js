const express = require('express');
const messageService = require('../service/message.service.js');
const back=require('../util/response');
const md5 = require('../util/md5.js');
var router = express.Router();
router.get('/getlist', function(req, res, next) {
    if(req.session.userToken){
        if(req.query.token!==md5.getToken(req.session.userToken)){
            back.jsonWrite(res, 500,"请先登录");
            return;
        }
    }
    else{
        back.jsonWrite(res, 500,"请先登录");
        return;
    }
    messageService.getlist(req,(state,msg,data,add)=>{
        back.jsonWrite(res, state,msg,data,add);
    })
});
router.get('/user/list',function (req,res,next){
    if(req.session.userToken){
        if(req.query.token!==md5.getToken(req.session.userToken)){
            back.jsonWrite(res, 500,"请先登录");
            return;
        }
    }
    else{
        back.jsonWrite(res, 500,"请先登录");
        return;
    }
    messageService.getMessageByFriend(req,(state,msg,data,add)=>{
        back.jsonWrite(res, state,msg,data,add);
    })
})
router.post('/message',function (req,res){
    if(req.session.userToken){
        if(req.body.token!==md5.getToken(req.session.userToken)){
            back.jsonWrite(res, 500,"请先登录");
            return;
        }
    }
    else{
        back.jsonWrite(res, 500,"请先登录");
        return;
    }
    messageService.saveMessage(req,(state,msg,data,add)=>{
        back.jsonWrite(res, state,msg,data,add);
    })
})
router.get('/count',(req,res)=>{
    messageService.getMessageCountByUser(req,(state,msg,data,add)=>{
        back.jsonWrite(res, state,msg,data,add);
    })
})
module.exports = router;