const express = require('express');
const adminService = require('../service/admin.service.js');
const back=require('../util/response');
const router = express.Router();
router.post('/dologin',(req,res)=>{
    adminService.login(req,(state,msg,data,add)=>{
        back.jsonWrite(res, state,msg,data,add);
    })
})
module.exports=router;