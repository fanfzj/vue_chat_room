var user={};
exports.getSocket =(server)=>{
    let io = require('socket.io')(server);
    io.sockets.on('connection',(socket)=>{
        console.log("用户："+socket.handshake.query.user_id+'链接成功'+','+socket.id);
        user[socket.handshake.query.user_id]=socket.id;
        socket.on('disconnect',function (){
            console.log('用户：'+socket.handshake.query.user_id+'断开链接')
            delete user[socket.handshake.query.user_id];
        })
        socket.on('sendMessage',(data)=>{
             if(socket.id && typeof user[data.user_id]!=='undefined'){
                 io.to(user[data.friend_id]).emit('receiveMessage',data);
             }
        })
    })
}