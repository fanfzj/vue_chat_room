const mysql=require('mysql');
const pool=mysql.createPool({
    host:'localhost',
    port:3306,
    database:'chat',
    user:'root',
    password:"root"
});
exports.query=(sql,callback_dao)=>{
    pool.getConnection((error,conn)=> {
        if(error){
            throw error;
            return;
        }
        conn.query(sql,(error,results)=>{
            conn.release();
            if(error){
                throw error;
            }
            callback_dao(results,error);
        });
    });
}
