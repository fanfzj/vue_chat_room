const crypto = require('crypto')
exports.getToken=(id)=>{
    let md5 = crypto.createHash('md5');
    md5.update(id);
    return md5.digest('hex');
}