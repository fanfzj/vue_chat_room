exports.jsonWrite = (res,state,msg,data={},extra={})=>{
    res.json({
        state:state,
        msg:msg,
        data:data,
        extra:extra
    })
}