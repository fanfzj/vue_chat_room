class admin {
    constructor(id,admin_account,admin_password){
        this.id=id;
        this.admin_account = admin_account;
        this.admin_password = admin_password;
    }
    getId(){
        return this.id;
    }
    setId(id){
        this.id=id;
    }
    getAdminAccount(){
        return this.admin_account;
    }
    setAdminAccount(admin_account){
        this.admin_account=admin_account;
    }
    getAdminPassword(){
        return this.admin_password;
    }
    setAdminPassword(admin_password){
        this.admin_password=admin_password;
    }
    print(){
        console.log(
            "id="+this.id+" "+
            "admin_account="+this.admin_account+" "+
            "admin_password="+this.admin_password
        );
    }
}
module.exports =admin;