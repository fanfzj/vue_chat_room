class message {
    constructor(id,send_user_id,receive_user_id,content){
        this.id=id;
        this.send_user_id = send_user_id;
        this.receive_user_id = receive_user_id;
        this.content = content;
    }
    getId(){
        return this.id;
    }
    setId(id){
        this.id=id;
    }
    getSendUserId(){
        return this.send_user_id;
    }
    setSendUserId(send_user_id){
        this.send_user_id=send_user_id;
    }
    getReceiveUserId(){
        return this.receive_user_id;
    }
    setReceiveUserId(receive_user_id){
        this.receive_user_id=receive_user_id;
    }
    getContent(){
        return this.content;
    }
    setContent(content){
        this.content=content;
    }
    print(){
        console.log(
            "id="+this.id+" "+
            "send_user_id="+this.send_user_id+" "+
            "receive_user_id="+this.receive_user_id+
            "content="+this.content
        );
    }
}
module.exports =message;