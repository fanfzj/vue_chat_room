class user {
    constructor(id,user_account,user_password,nickname,head_img,sex,mobile_number,del){
        this.id=id;
        this.user_account = user_account;
        this.user_password = user_password;
        this.nickname = nickname;
        this.head_img = head_img;
        this.sex = sex;
        this.mobile_number = mobile_number;
        this.del=del;
    }
    getId(){
        return this.id;
    }
    setId(id){
        this.id=id;
    }
    getUserAccount(){
        return this.user_account;
    }
    setUserAccount(user_account){
        this.user_account=user_account;
    }
    getHeadImg(){
        return this.head_img;
    }
    setHeadImg(head_img){
        this.head_img=head_img;
    }
    print(){
        console.log(
            "id="+this.id+" "+
            "User_account="+this.user_account+" "+
            "nickname="+this.nickname
        );
    }
}
module.exports =user;